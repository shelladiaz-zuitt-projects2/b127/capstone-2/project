const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');

//Route for checking if the user's email already exists in the database
router.post('/checkEmail', (req, res) =>{
	userController.checkEmailExists(req.body).then(result => res.send(result));
})


//route for registration
router.post('/register', (req, res) => {
    userController.registerUser(req.body).then(result => res.send(result));
})

//user login
router.post('/login', (req, res) => {
    userController.loginUser(req.body).then(result => res.send(result))
})
   
//Set User as admin
router.put('/:userId/setAdmin', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    if(data.isAdmin){
        userController.setAdmin(req.params).then(result => res.send(result));
    }else{
        res.send(false);
    }
})   

//view all details
router.get('/mydetails', auth.verify, (req, res) => { 

    const userData = auth.decode(req.headers.authorization)
   

    userController.myDetails({userId: userData.id}).then(result => res.send(result))

})


//view orders per person
router.get('/view', auth.verify, (req, res) => { 

    const userData = auth.decode(req.headers.authorization)
   

    userController.myOrders({userId: userData.id}).then(result => res.send(result))

})


//Admin view all orders
router.get('/allOrder', auth.verify, (req, res) => {
    let data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        
    } 

    if(data.isAdmin){
        userController.viewOrders().then(result => res.send(result));
    }else{
        res.send(false);
    }
    
})

//update user profile
router.put('/update', auth.verify, (req, res) => { 


    const userData = auth.decode(req.headers.authorization)
   

    userController.updateProfile({userId: userData.id}, req.body).then(result => res.send(result))

})



  
 

 



module.exports = router;
