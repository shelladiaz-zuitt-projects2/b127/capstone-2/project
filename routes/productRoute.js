const express = require('express');
const router = express.Router();
const productControllers = require('../controllers/productControllers');
const auth = require('../auth');

//Creation of product
router.post('/', auth.verify, (req, res) => {
    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin){
        productControllers.addProduct(data).then(result => res.send(result));
    }else{
        return false;
    }
})
   

//Normal User Retrieve all active product
router.get('/', (req, res) => {
    productControllers.getAllActive().then(result => res.send(result));
})

//Normal User Retrieve all active product
router.get('/all', (req, res) => {
    productControllers.getAllProducts().then(result => res.send(result));
})

//Retrieve single product
router.get('/:productId', (req, res) => {
    productControllers.specificProduct(req.params).then(result => res.send(result));
})

//Update Product
router.put('/:productId', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin){
        productControllers.updateProduct(req.params, req.body).then(result => res.send(result));
    }else{
        return false;
    }
})
 

//Archived Product
router.put('/:productId/archive', auth.verify, (req,res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    if(data.isAdmin){
        productControllers.archiveProduct(req.params).then(result => res.send(result));
    }else{
        return false;
    }
})

//Activate a product
router.put('/:productId/activate', auth.verify, (req, res) =>{
    productControllers.activateProduct(req.params).then(result => res.send(result));
})

module.exports = router;