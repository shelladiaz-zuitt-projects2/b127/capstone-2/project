const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderControllers');
const auth = require('../auth'); 


//add to cart
router.post('/checkout', auth.verify, (req, res) => {
    let data = {

        userId: auth.decode(req.headers.authorization).id,
        product: req.body
    }

    orderController.checkout(data).then(result => res.send(result))
}) 

router.post('/addOrder', auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}

	if(!data.isAdmin){
		orderController.addOrder(data).then(result => res.send(result))
	}else {
		res.send(false)
	}

})

router.get('/allOrders', auth.verify, (req,res) =>  {
	const data =  {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if (data.isAdmin) {
		orderController.allOrders().then(result => res.send(result))
	}else {
		res.send(false);
	}
})


router.get('/myOrders', (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}
	if (!data.isAdmin) {
		orderController.myOrders(data).then(result => res.send(result))
	} else {
		res.send(false);
	}
})


 


module.exports = router; 