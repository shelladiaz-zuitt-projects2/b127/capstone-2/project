const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Product = require('../models/Product');

module.exports.checkEmailExists = (reqBody) => {
	return User.find( { email: reqBody.email } ).then(result => {
		//The "find" mthod returns a record if a match is found
		if(result.length > 0){
			return true;
		}else{
			//No duplicate email found
			//The user is not yet registered in the database
			return false;
		}
	})
}

//User Registration
/*
Steps:
1. Create a new User object using the mongoose model and the information from the request body.
2. Error handling, if error, return error. else, Save the new User to the database

*/
module.exports.registerUser = (reqBody) =>{

    //Uses the information from the request body to provide all necessary information
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        address: reqBody.address,
        password: bcrypt.hashSync(reqBody.password, 10)
    })
    
    
        //Saves the created object to our database
        return newUser.save().then((user, error) => {
            //User registration failed
            if(error){
                return false;
            }else{
                //User registration is successful
                return true;
            }
        })
    }
 
//User Registration
// module.exports.registerUser = (reqBody) =>{

//     return User.find( {email: reqBody.email } ).then((result) => {
//         if(result.length > 0){
//             return {message: "Email already exist!"};
//         }else{
 
//             let newUser = new User({
//                 firstName: reqBody.firstName,
//                 lastName: reqBody.lastName,
//                 email: reqBody.email,
//                 mobileNo: reqBody.mobileNo,
//                 address: reqBody.address,
//                 password: bcrypt.hashSync(reqBody.password, 10)
//             })
           
//             return newUser.save().then((user, error) => {
//                 if(error){
//                     return {message: "Registration Error!"};
//                 }else{
//                     return {message: "Registration Successful!"};
//                 }
//             })
        
//         }
//     })   
// }

//Login User

module.exports.loginUser = (reqBody) => {
    return User.findOne( {email: reqBody.email} ).then(result => {
        if(result == null){
            return { message: "User doesn't exist!" }
        }else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect){
                return { accessToken: auth.createAccessToken(result.toObject()) }
            }else{
                return false;
            }
        }
    })
} 

//Set User as admin
module.exports.setAdmin = (reqParams) => {
    let updateAdmin = {
        isAdmin: true
    }

    return User.findByIdAndUpdate(reqParams.userId, updateAdmin).then((update, err)=> {
        if(err){
            return false;
        }else{
            return true;
        }
    }) 
} 

//view details per person
module.exports.myDetails = (data) => {

    return User.findById(data.userId).then((result, err) => {
       if(err){
           return false;
       }else{
          return result;
       }
    })
}

//view orders per person
module.exports.myOrders = (data) => {

    return User.findById(data.userId).then((result, err) => {
       if(err){
           return false;
       }else{
          return result.transaction;
       }
    })
}

//Admin view all orders
module.exports.viewOrders = () => {
    return User.find({isAdmin: false}).then((result, err) => {
        return result;
    })
}



//update user profile
module.exports.updateProfile = (data, reqBody) => {

    let updateProfile ={
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        address: reqBody.address
    }

    return User.findByIdAndUpdate(data.userId, updateProfile).then((profile, error) => {
        if(error){
            return false;
        }else{
            return true;
        }
    })
}
  