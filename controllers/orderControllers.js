const Product = require('../models/Product');
const User = require('../models/User');
const auth = require('../auth');


// add to cart
module.exports.checkout = async (data) => {

    let productPrice = await Product.findById(data.product.productId).then(product => {
        return product.price;
    })

    let productName = await Product.findById(data.product.productId).then(product => {
        return product.name;
    })

    console.log(productName);

    let isUserUpdated = await User.findById(data.userId).then(user => {

        const totalAmount = (productPrice * data.product.quantity);


        user.transaction.push({
            productId: data.product.productId,
            quantity: data.product.quantity,
            name: productName,
            productPrice: productPrice,
            totalAmount: totalAmount
        });
  

        return user.save().then((user, error) => {
            if(error){
                return false;
            }else{

            return true;
                     
            }
        })
    })  

    let isProductUpdated = await Product.findById(data.product.productId).then(product => {
              
        const totalAmount = productPrice * data.product.quantity;
      
        product.checkout.push({
            userId: data.userId, 
            quantity: data.product.quantity,
            name: productName,
            totalAmount: totalAmount
           
        });

        return product.save().then((product, error) => {
            if(error){
                return false;
            }else{
                return true;
        
            }
        })
    }) 

    if(isUserUpdated && isProductUpdated){

        return true;
       
    }else{
        return false;
    }
}

module.exports.addOrder = (data) => {
	

		if (result !== null) {
			let newOrder = new Order({
				totalPrice: result.totalPrice,
				products: result.products,
				userId: data.userId
			})
	
			return newOrder.save().then((result,isError)=> {
				return(isError)? false: true;
			})
		} else {
			return false;
		}


}

module.exports.allOrders = () => {
	return Order.find({}).then(result => {
		return result
	})
}


module.exports.myOrders = (data) => {
	return Order.find({userId: data.userId}).then(result =>{
		let productArr=[];
		result.forEach(item=>{
			productArr.push(item.products)
		})
		return productArr.flat();
	});
}




