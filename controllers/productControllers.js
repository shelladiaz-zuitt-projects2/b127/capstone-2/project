const Product = require('../models/Product');



//Creation of Product
module.exports.addProduct = (data) => {
  
    let newProduct = new Product({
        name: data.product.name,
        description: data.product.description,
        price: data.product.price,
    })

    return newProduct.save().then((product, error) => {
        if(error){
            return false;
        }else{
            return true;
        }
    }) 
 
}  



//Retrieve all active product
module.exports.getAllActive = () => {
    return Product.find({isActive: true}).then(result => {
        
        return result;
    })
}

//Retrieve all  product
module.exports.getAllProducts = () => {
    return Product.find({}).then(result => {
        
        return result;
    })
}


//Retrieve single product 
module.exports.specificProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result;
    })
} 

//Update Product
module.exports.updateProduct = (reqParams, reqBody) => {

    let updateProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,

    }

    return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((course, error) => {
        if(error){
            return false;
        }else{
            return true;
        }
    })
}

//Archived Product
module.exports.archiveProduct = (reqParams) => {

    let updateProduct = {
        isActive: false
    }

    return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, err) => {
        if(err){
            return false;
        }else{
            return  true;
        }
    })
}

//activate a product
module.exports.activateProduct = (params) => {
	const updates = { isActive: true }

	return Product.findByIdAndUpdate(params.productId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}


