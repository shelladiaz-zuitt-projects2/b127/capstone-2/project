const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required : [true, "Product name is required"]
    },
    description: {
        type: String,
        required : [true, "Description is required"]
    },
    price: {
        type: Number,
        required : [true, "Price is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn : {
        type: Date,
        default: new Date()
    },
    // availableStock: {
    //     type: Number,
    //     required : [true, "availableStock is required"]
    // },
    checkout : [
        {
            userId: {
                type: String,
                required: [true, "UserId is required"]
            },
            
            quantity: {
                type: Number,
                min: 0
            },
            name : {
				type: String,
				required: [true, "Product name is required"]
			},

            totalAmount: {
                type: Number,
                min: 0

            },
            orderOn: {
                type: Date,
                default: new Date()
            },
        }
    ]
    
    
})

module.exports = mongoose.model("Product", productSchema)