const mongoose = require('mongoose');


const orderSchema = new mongoose.Schema({

	totalPrice: {
		type: Number
	},
    userId:{
		type: String,
		required: [true, "UserId is required"]
	},
	products : [
		{	
            productId : {
				type: String,
				required: [true, "ProductId is required"]
			},
			name : {
				type: String,
				required: [true, "Productnameis required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"],
				default: 1
			},
			totalPerProduct: {
				type: Number
			},purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]

	
	
})

module.exports = mongoose.model("Order", orderSchema)
