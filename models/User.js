const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required : [true, "First Name is required"]
    },
    lastName: {
        type: String,
        required : [true, "Last Name is required"]
    },
    email: {
        type: String,
        required : [true, "Email is required"]
    },
    password: {
        type: String,
        required : [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required : [true, "Mobile Number is required"]
    },
    address: {
        type: String,
        required : [true, "Address is required"]
    },
    transaction: [
        {
            productId: {
                type: String,
                required: [true, "productId is required"]
            },
            name : {
				type: String,
				required: [true, "Product name is required"]
			},

            quantity: {
                type: Number,
                min: 0
            },
            productPrice: {
                type: Number,
                min: 0
            },
            totalAmount: {
                type: Number,
                min: 0
            },
            orderOn: {
                type: Date,
                default: new Date()
            }
              
            
        }
    ]
    
})

module.exports = mongoose.model("User", userSchema);