const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

/**List of Routes */
const userRoutes = require('./routes/userRoute')
const productRoutes = require('./routes/productRoute')
const orderRoutes = require('./routes/orderRoute')




const app = express();

//Connect to our MongoDB database
mongoose.connect('mongodb+srv://admin:admin@zuitt-bootcamp.dxzdo.mongodb.net/Batch127_Ecommerce?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})


mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

/**Based Routes */
//"http://localhost:4000/users"
app.use('/users', userRoutes); 
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);





app.listen(process.env.PORT || 4000, ()=> {
	console.log(`API is now online on port ${ process.env.PORT || 4000}`)
})
